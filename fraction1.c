#include<stdio.h>
struct frac
{
 int n3,d3;
};
int input()
{
 int a;
 scanf("%d",&a);
 return a;
}
int gcd(int a,int b)
{
 int i,g;
 for(i=1;i<a&&i<b;i++)
 {
  if(a%i==0&&b%i==0)
  g=i;
  return g;
 }
}
struct frac compute(int n1,int d1,int n2,int d2,int g)
{
 struct frac p1;
 p1.d3=gcd(d1,d2);
 p1.d3=(d1*d2)/p1.d3;
 p1.n3=(n1)*(p1.d3/d1) + (n2)*(p1.d3/d2);
 return p1;
}
void output(int n1,int d1,int n2,int d2,struct frac p2)
{
 printf("The sum of %d/%d and %d/%d is %d/%d",n1,d1,n2,d2,p2.n3,p2.d3);
}
int main()
{
 int n1,d1,n2,d2,g;
 printf("Enter the numerator\n");
 n1=input();
 printf("Enter the denominator\n");
 d1=input();
 printf("Enter the numerator\n");
 n2=input();
 printf("Enter the denominator\n");
 d2=input();
 g=gcd(d1,d2);
 struct frac p3=compute(n1,d1,n2,d2,g);
 output(n1,d1,n2,d2,p3);
 return 0;
}
